<h1 align="center">MV-Login</h1> <br>
<!--<p align="center">
    <img alt="mv-udc logo" title="mv-login logo" src="#" width="300">
</p>-->
<div align="center">
  <strong>MV-Login</strong>
</div>
<div align="center">
  Login kiosk app for Valais Media Library.
</div>

<div align="center">
  <h3>
    <a href="https://valais-media-library.gitlab.io/mv-login/">Access</a>
    <span> | </span>
    <a href="#documentation">Documentation</a>
    <span> | </span>
    <a href="#contributing">
      Contributing
    </a>
  </h3>
</div>

<div align="center">
  <sub>Built with ❤︎ in Valais by <a href="https://gitlab.com/valais-media-library/mv-login/-/graphs/master">contributors</a>
</div>


## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Usage](#usage)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [History and changelog](#history-and-changelog)
- [Authors and acknowledgment](#authors-and-acknowledgment)

## Introduction

[![license](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)](https://gitlab.com/valais-media-library/mv-login/blob/master/LICENSE)


## Features

- Kiosk mode
- Linked to RERO oAuth
- Multi-platform

## Documentation


### Install

*macOS 10.10+, Linux, and Windows 7+ are supported (64-bit only).*

**macOS**

[**Download**](https://gitlab.com/valais-media-library/mv-login/-/tree/master/dist/) the `.dmg` file.

**Linux**

[**Download**](https://gitlab.com/valais-media-library/mv-login/-/tree/master/dist/) the `.AppImage` or `.deb` file.

*The AppImage needs to be [made executable](http://discourse.appimage.org/t/how-to-make-an-appimage-executable/80) after download.*

**Windows**

[**Download**](https://gitlab.com/valais-media-library/mv-login/-/tree/master/dist/) the `.exe` file.

### Configure

Once installed, open the app and press `CmdOrCtrl+Shift+P`. Open `preferences.json` and edit data as this :

```
{
    "path": {
        "auth-url": "https://services-mv.xxx.ch/xxx.php",
        "log-url": "http://xml.xxx.ch/xxx/xxx/xxx.php",
        "log-password": "xxx",
        "computer-name": "xxx"
    }
}
```
---


### Dev

Built with [Electron](https://electronjs.org).

### Run

```
$ npm install
$ npm start
```

### Build app

```
$ npm run dist
```

### Publish

```
$ npm run release
```

After Travis finishes building your app, open the release draft it created and click "Publish".

## Contributing

We’re really happy to accept contributions from the community, that’s the main reason why we open-sourced it! There are many ways to contribute, even if you’re not a technical person. Check the [roadmap](#roadmap) or [create an issue](https://gitlab.com/valais-media-library/mv-udc/issues) if you have a question.

1. [Fork](https://help.github.com/articles/fork-a-repo/) this [project](https://gitlab.com/valais-media-library/mv-login/)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## History and changelog

You will find changelogs and releases in the [CHANGELOG](https://gitlab.com/valais-media-library/mv-login/blob/master/CHANGELOG) file.

## Roadmap

Nothing

## Authors and acknowledgment

* **Michael Ravedoni** - *Initial work* - [michaelravedoni](https://gitlab.com/michaelravedoni)

See also the list of [contributors](https://gitlab.com/valais-media-library/mv-login/-/graphs/master) who participated in this project.

* [Electron](https://www.electronjs.org/) Build cross-platform desktop apps with JavaScript, HTML, and CSS.

## License

[MIT License](https://gitlab.com/valais-media-library/mv-login/blob/master/LICENSE)
