'use strict';
const path = require('path');
const {app, BrowserWindow, globalShortcut, Menu, ipcMain, shell} = require('electron');
const {autoUpdater} = require('electron-updater');
const {is} = require('electron-util');
const unhandled = require('electron-unhandled');
/* Const debug = require('electron-debug'); */
const contextMenu = require('electron-context-menu');
const config = require('./config');
const menu = require('./menu');
/* Const packageJson = require('./package.json'); */
const os = require('os');
const ElectronPreferences = require('electron-preferences');
const prompt = require('electron-prompt');

unhandled();
/* Debug(); */
contextMenu();

/* App.setAppUserModelId(packageJson.build.appId); */
app.setAppUserModelId('com.valais-media-library.mv-login');

// Uncomment this before publishing your first version.
// It's commented out as it throws an error if there are no published versions.
// if (!is.development) {
//	const FOUR_HOURS = 1000 * 60 * 60 * 4;
//	setInterval(() => {
//		autoUpdater.checkForUpdates();
//	}, FOUR_HOURS);
//
//	autoUpdater.checkForUpdates();
// }

const preferences = new ElectronPreferences({
    /**
     * Where should preferences be saved?
     */
    'dataStore': path.resolve(app.getPath('userData'), 'preferences.json'),
    /**
     * Default values.
     */
    'defaults': {},
    /**
     * If the `onLoad` method is specified, this function will be called immediately after
     * preferences are loaded for the first time. The return value of this method will be stored as the
     * preferences object.
     */
    'onLoad': (preferences) => {
        // ...
        return preferences;
    },
    /**
     * The preferences window is divided into sections. Each section has a label, an icon, and one or
     * more fields associated with it. Each section should also be given a unique ID.
     */
     'sections': [
         {
             'id': 'path',
             'label': 'Path',
             /**
             * See the list of available icons below.
             */
             'icon': 'key-25',
             'form': {
                 'groups': [
                     {
                         /**
                         * Group heading is optional.
                         */
                         'label': 'Authentification',
                         'fields': [
                             {
                                 'label': 'Authentification URL',
                                 'key': 'auth-url',
                                 'type': 'text',
                                 'help': 'https://services-mv.xxx.ch/xxx.php'
                             },
                             {
                                 'label': 'Server log URL',
                                 'key': 'log-url',
                                 'type': 'text',
                                 'help': 'http://xml.xxx.ch/outils/posteLogin/xxx.php'
                             },
                             {
                                 'label': 'Server log password',
                                 'key': 'log-password',
                                 'type': 'text',
                                 'help': ''
                             },
                             {
                                 'label': 'Computer name',
                                 'key': 'computer-name',
                                 'type': 'text',
                                 'help': 'Name of the machine/computer'
                             },
                             {
                                 'heading': 'Relaunch the application',
                                 'content': '<p>To relaunch the app, press `CmdOrCtrl+Shift+R`. Press `CmdOrCtrl+Shift+E` to close.</p>',
                                 'type': 'message',
                             }
                         ]
                     }
                 ]
             }
         },
         {
            'id': 'application',
            'label': 'Application',
            /**
            * See the list of available icons below.
            */
            'icon': 'widget',
            'form': {
                'groups': [
                    {
                        'label': 'Passwords',
                        'fields': [
                            {
                                'label': 'Preferences password',
                                'key': 'preferences_secret',
                                'type': 'text',
                                'help': 'Password is `secret` if not set. Then Press `CmdOrCtrl+Shift+R`.'
                            },
                            {
                                'heading': 'Relaunch the application',
                                'content': '<p>To relaunch the app, press `CmdOrCtrl+Shift+R`. Press `CmdOrCtrl+Shift+E` to close.</p>',
                                'type': 'message',
                            }
                        ]
                    }
                ]
            }
        }
     ]
 });

// Prevent window from being garbage collected
let mainWindow = 'login';
let loginWindow;
let sessionWindow;

const createloginWindow = async () => {
	const win = new BrowserWindow({
		title: app.name,
		fullscreen: true,
        frame: false,
		kiosk: true,
        autoHideMenuBar: true,
		closable: false,
		showInTaskbar: true,
		alwaysOnTop: true,
		focusable: true,
		webPreferences: {
			preload: path.join(app.getAppPath(), 'preload.js')
		}
	});

	globalShortcut.register('CmdOrCtrl+Shift+E', () => {
		app.exit();
	});

	globalShortcut.register('CmdOrCtrl+Shift+R', () => {
		app.relaunch();
		app.exit();
	});

    globalShortcut.register('CmdOrCtrl+Shift+I', () => {
        win.setKiosk(false);
        //config.openInEditor();
        shell.openItem(app.getPath('userData'));
    });

    globalShortcut.register('CmdOrCtrl+Shift+P', () => {
        prompt({
            title: 'Enter a password',
            label: 'Password',
            value: null,
            inputAttrs: {
                type: 'password'
            },
            type: 'input'
        }, win)
        .then((r) => {
            const secret = preferences.value('application.preferences_secret') || 'secret';
            if(r === secret) {
                console.log('ok');
                win.setKiosk(false);
                win.destroy();
                preferences.show();
            } else {
                console.log('Password incorrect: ', r);
            }
        })
        .catch(console.error);
    });

	win.on('ready-to-show', () => {
		win.show();
	});

    win.onbeforeunload = (e) => {
        console.log('I do not want to be closed')
        e.returnValue = false;
    }

    win.on('close', (e) => {
        console.log('I do not want to be closed')
        e.preventDefault();
    });

	win.on('closed', () => {
		// Dereference the window
		// For multiple windows store them in an array
		/* LoginWindow = undefined; */
	});

	await win.loadFile(path.join(__dirname, 'login.html'));

	return win;
};

const createSessionWindow = async () => {
	const win = new BrowserWindow({
		title: app.name,
		show: false,
		width: 768,
		height: 500,
		closable: false,
		kiosk: false,
        autoHideMenuBar: true,
		showInTaskbar: true,
		alwaysOnTop: false,
		focusable: true,
		minimizable: true,
		maximizable: false,
		webPreferences: {
			preload: path.join(app.getAppPath(), 'preload.js')
		}
	});

    globalShortcut.register('CmdOrCtrl+Shift+P', () => {
        preferences.show();
    });

	win.on('ready-to-show', () => {
		win.show();
	});

    win.onbeforeunload = (e) => {
        console.log('I do not want to be closed')
        e.returnValue = false;
    }

    win.on('close', (e) => {
        console.log('I do not want to be closed')
        e.preventDefault();
    });

	win.on('closed', () => {
		// Dereference the window
		// For multiple windows store them in an array
		sessionWindow = undefined;
	});

	await win.loadFile(path.join(__dirname, 'session.html'));

	return win;
};

// Prevent multiple instances of the app
if (!app.requestSingleInstanceLock()) {
	app.quit();
}

app.on('second-instance', () => {
	if (sessionWindow || mainWindow == 'session') {
		if (sessionWindow.isMinimized()) {
			sessionWindow.restore();
		}

		sessionWindow.show();
	}
});

app.on('window-all-closed', () => {
	if (!is.macos) {
		app.quit();
	}
});

app.on('activate', () => {
	if (mainWindow == 'login' && !loginWindow) {
		loginWindow = createloginWindow();
	} else if (mainWindow == 'session' && !sessionWindow) {
		sessionWindow = createSessionWindow();
	}
});

ipcMain.on('user-authentificated', (event, card) => {
	console.log('user-authentificated received');
	sessionWindow = createSessionWindow();
	mainWindow = 'session';
	global.sharedObj = {userCard: card};
});

ipcMain.on('user-logout', (event, card) => {
	console.log('user-logout received');
	loginWindow = createloginWindow();
	mainWindow = 'login';
	global.sharedObj = {userCard: null};
});

(async () => {
	await app.whenReady();
	Menu.setApplicationMenu(menu);
	loginWindow = await createloginWindow();
})();
