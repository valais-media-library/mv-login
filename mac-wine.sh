#!/bin/sh

mkdir -p /tmp/wine-stage/wine/usr
cd /tmp/wine-stage/wine/usr
open .
echo "Download : https://github.com/electron-userland/electron-builder-binaries/releases/download/wine-2.0.1-mac-10.12/wine-2.0.1-mac-10.12.7z"
open "https://github.com/electron-userland/electron-builder-binaries/releases/download/wine-2.0.1-mac-10.12/wine-2.0.1-mac-10.12.7z"
echo "unpack 'wine-2.0.1-mac-10.12/wine-2.0.1-mac-10.12.7z', then go to /tmp/wine-stage/wine/usr
and copy in the wine files such that the lookup for ntdll.dll.so is satisfied."